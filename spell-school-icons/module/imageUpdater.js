import { logMessage } from "./utils.js"

/**
 * Provides functionality for changing spell images
 */
export class ImageUpdater {

  /**
   * The base path for all included images
   */
  static baseIconPath = "modules/spell-school-icons/icons/";

  /**
   * Map each of the spell schools to an image
   */
  static iconMap = (school) => ({
    "abj": "abjuration.png",
    "con": "conjuration.png",
    "div": "divination.png",
    "enc": "enchantment.png",
    "evo": "evocation.png",
    "ill": "illusion.png",
    "nec": "necromancy.png",
    "trs": "transmutation.png"
  })[school]

  /**
   * Map the selected set from settings to a icon set folder
   */
  static folderMap = (set) => ({
    0: "white/",
    1: "black/"
  })[set]

  /**
   * Checks to see if we should change the image of the spell being updated
   *  If so, updates it according to the spell's school
   * @param {Object} spell - the item being updated 
   * @param {Object} updateData - the current set of data being updated
   */
  static updateSpell(spell, updateData) {
    if ("school" in updateData.data && spell.type == "spell") {
      if (this.shouldChangeIcon(spell)) {
        updateData.img = this.getIconPath(updateData.data.school);
        logMessage(`Spell "${spell.name}" updated to image: ${spell.img}`);
      }
    }
  }

  /**
   * Sets the image of a newly created spell to the default (abjuration) image
   * @param {Object} spell - the spell being created
   */
  static createSpell(spell) {
    if (spell.type == "spell") {
      if (this.shouldChangeIcon(spell))
        spell.img = this.getFallbackImagePath();
        logMessage(`Spell "${spell.name}" created with image: ${spell.img}`);
    }
  }

  /**
   * Attempts to run an update on all spells in an Actor's spellbook
   * @param {Object} actor The actor whose spell book needs to be updated
   */
  static bulkUpdate(actor) {
    logMessage(`Running a bulk update for actor: ${actor.actor.name}`);
    var t0 = performance.now();    

    if ("spellbook" in actor) {
      actor.spellbook.forEach(spellLevel => {
        logMessage(`Updating level ${spellLevel.level} spells`)
        spellLevel.spells.forEach(spell => {
          logMessage(`Updating level ${spell.data.level} spell "${spell.name}"`);
          if (this.shouldChangeIcon(spell)) {
            if (spell.data.school === undefined || spell.data.school == "") {
              spell.img = this.getFallbackImagePath();
            } else {
              spell.img = this.getIconPath(spell.data.school);
            }
            logMessage(`Spell "${spell.name}" updated to image: ${spell.img}`);
          }
        })
      });

      ui.notifications.notify(`Bulk update complete. Please re-open the sheet to see changes.`)
    }

    logMessage(`Finished bulk update in ${(performance.now() - t0)} milliseconds`);
  }

  /**
   * Determines if the image should be changed, based on the current image
   * @param {Object} spell - The spell being questioned
   */
  static shouldChangeIcon(spell) {
    // is it the mystery man? change it
    var fileName = spell.img.replace(/^.*[\\\/]/, '');
    var set = game.settings.get("spell-school-icons", "icon-set");
    if (fileName == "mystery-man.svg") {
      return true;
    }
    // is it already the right image? dont change it
    else if (fileName == this.iconMap(spell.school)) {
      return false;
    }
    // is it some other school icon? change it
    else if (spell.img.startsWith(this.baseIconPath)) {
      return true;
    }
    else return false;
  }

  /**
   * Gets the path to the default (abjuration) image path
   */
  static getFallbackImagePath() {
    return this.getIconPath("abj");
  }

  /**
   * Gets the image path for a spell based on the spell school
   * @param {string} school - the school of the spell
   */
  static getIconPath(school) {
    var set = game.settings.get("spell-school-icons", "icon-set");
    return this.baseIconPath + this.folderMap(set) + this.iconMap(school);
  }
}
