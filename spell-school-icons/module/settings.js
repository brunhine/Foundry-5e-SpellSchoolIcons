export const modName = "Spell School Icons";

/**
 * Register all of the settings used by the module
 */
export const registerSettings = function () {

    /**
     * Icon Set Options
     */
    game.settings.register("spell-school-icons", "icon-set", {
        name: "spell-school-icons.icon-set.name",
        hint: "spell-school-icons.icon-set.hint",
        scope: "world",
        config: true,
        type: Number,
        default: 0,
        choices: [
            "spell-school-icons.icon-set.white",
            "spell-school-icons.icon-set.black"
        ]
    });

    /**
     * Bulk Update Mode
     */
    game.settings.register("spell-school-icons", "bulk-mode", {
        name: "spell-school-icons.bulk-mode.name",
        hint: "spell-school-icons.bulk-mode.hint",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });
    game.settings.set("spell-school-icons", "bulk-mode", false);

    /**
     * Debug Mode
     */
    game.settings.register("spell-school-icons", "debug", {
        name: "spell-school-icons.debug.name",
        hint: "spell-school-icons.debug.hint",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });

};