import { ImageUpdater } from "./module/imageUpdater.js"
import { registerSettings, modName } from "./module/settings.js"
import { logMessage } from "./module/utils.js"

/**
 * Initialize module
 */
Hooks.once("init", async function () {
  registerSettings();
  logMessage(`Settings Initialized`, true);
})

/**
 * Listen for unowned spells to be created
 */
Hooks.on("preCreateItem", (spell) => {
  ImageUpdater.createSpell(spell);
});

/**
 * Listen for owned spells to be created
 */
Hooks.on("createOwnedItem", (actor, spell) => {
  ImageUpdater.createSpell(spell);
});

/**
 * Listen for unowned spells to be updated
 */
Hooks.on("preUpdateItem", (spell, updateData) => {
  ImageUpdater.updateSpell(spell, updateData);
});

/**
 * Listen for owned spells to be updated
 */
Hooks.on("preUpdateOwnedItem", (actor, spell, updateData) => {
  ImageUpdater.updateSpell(spell, updateData);
});

/**
 * Listen for an Actor sheet to be opened, if we are in bulk update mode, run a bulk update
 */
Hooks.on("renderActorSheet", (app, html, actor) => {
  if (game.settings.get("spell-school-icons", "bulk-mode")) {
    ImageUpdater.bulkUpdate(actor);
  }
});
