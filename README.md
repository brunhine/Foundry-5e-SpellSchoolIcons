# Spell School Icons
**Spell School Icons** is a module for [Foundry VTT](https://foundryvtt.com/ "Foundry VTT") that replaces the default item icon for spells with an icon representing the school that spell is from. This module is built specifically for the D&D 5e system.

## Installation
To install this module:
1.  Inside the Foundry "Configuration and Setup" screen, click "Add-on Modules"
2.  Click "Install Module"
3.  In the "Manifest URL" field, paste the following url:
`https://gitlab.com/brunhine/Foundry-5e-SpellSchoolIcons/raw/master/spell-school-icons/module.json`
4.  Click 'Install' and wait for installation to complete
5.  Don't forget to enable the module in game using the "Manage Module" button

## Usage
In any spell window, changing the spell's school will update the icon to the appropriate image. This module will not attempt to overwrite images already assigned to the spell unless it has been previously set by this module.
<div align="center">
![example1](/examples/example1.gif?raw=true)
</div>

### Icon Sets
Multiple icon sets are included, and more are to be added
##### Minimal (both black and white versions included)
| abjuration | conjuration | divination | enchantment | evocation | illusion | necromancy | transmutation |
|----| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| ![](/spell-school-icons/icons/black/abjuration.png?raw=true) | ![](/spell-school-icons/icons/black/conjuration.png?raw=true) | ![](/spell-school-icons/icons/black/divination.png?raw=true) | ![](/spell-school-icons/icons/black/enchantment.png?raw=true) | ![](/spell-school-icons/icons/black/evocation.png?raw=true) | ![](/spell-school-icons/icons/black/illusion.png?raw=true) | ![](/spell-school-icons/icons/black/necromancy.png?raw=true) | ![](/spell-school-icons/icons/black/transmutation.png?raw=true) |




## Compatibility
Most recently tested on [Foundry VTT](https://foundryvtt.com/ "Foundry VTT") version 0.5.5 (beta).

## Feedback
All feedback and suggestions are welcome. Please contact me on Discord (Brunhine#2182).

Any issues, bugs, or feature requests are always welcome to be reported directly to the [Issue Tracker](http://https://gitlab.com/brunhine/Foundry-5e-SpellSchoolIcons/-/issues "Issue Tracker")

## License

Spell School Icons is a module for [Foundry VTT](https://foundryvtt.com/ "Foundry VTT") by Jeremiah Altepeter and is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](https://foundryvtt.com/article/license/).

The "Minimal" icon set was created by [Dan Connolly](http://www.danconnolly.co.uk/) and is used with permission