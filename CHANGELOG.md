### 0.9.1
- Add ability to "Bulk Update" sheets
- Change default icon pack to "White Minimal"
- Add console debug functionality

### 0.9
- Initial Foundry Release 